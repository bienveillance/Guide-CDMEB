---
title: "Organisation d'un sondage de date"
menuTitle: "Sondage de date"
description: ""
---

Un sondage de date permet aux membres d'un groupe de travail de décider d'une date de réunion.

Nous utilisons [Framadate](http://framadate.org/), porté par une association d'éducation populaire (Framasoft) et s'appuyant sur un logiciel libre. Il est préférable d'utiliser [Framadate](http://framadate.org/) plutôt que Doodle qui récolte les données personnelles (dont le courriel) et peu respectueux de la vie privée.

De plus, Framadate offre une option inexistante chez Doodle en plus du oui/non : le "s'il le faut je me libère", qui s'avère très pratique !

* qualité du service : excellent
