---
title: "Organisation d'une conférence téléphonique"
menuTitle: "Conférence téléphonique"
description: ""
---

Une conférence téléphonique permet à plusieurs personnes d'appeler un numéro donné, et de d'échanger.

OVH propose cela gratuitement, moyennant une inscription par courriel. Le numéro est ouvert pour 24h et peut accueillir jusqu'à 50 personnes.

[Créer une conférence téléphonique OVH](https://www.ovh.com/cgi-bin/telephony/webconf.pl)

* qualité du service : excellent
