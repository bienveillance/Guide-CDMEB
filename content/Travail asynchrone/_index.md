---
title: "Le Travail asynchrone"
date: 2017-12-19T15:51:47+01:00
weight: 10
chapter: true
pre: "<b>3. </b>"
---

# Travail asynchrone

Le travail asynchrone est constitué de tous les **outils et méthodes** permettant de faire avancer des projets en travaillant
**à plusieurs, à distance**, et **chacun à son rythme**.

{{% children depth="5" sort="weight" %}}