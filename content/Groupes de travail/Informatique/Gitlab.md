---
title: "Gitlab"
date:  2018-02-10T18:39:12+01:00
weight: 5
---

## Se créer un compte pour Gitlab

1. Se créer un compte sur [Gitlab](https://gitlab.com/users/sign_in#register-pane):
    * *Full name* : le nom complet (exemple : `Jean-Philippe COLAS`)
    * *Username* : l'identifiant (exemple : `jpcolas`)
    * *Email* : votre adresse mail
    * *Password* : le mot de passe que vous choisissez pour Gitlab
2. Suivez les instructions pour activer votre compte
3. Envoyez votre *Username* à `technique AROBASE cdmeb.org` (remplacez `AROBASE` par `@`)

## Accéder à la gestion de projet sur GitLab

C'est par ici : [Projets CDMEB](https://gitlab.com/Glenans-CDMEB/Projets). Il est aussi possible de regarder les tâches avec :

* [la vue en liste](https://gitlab.com/Glenans-CDMEB/Projets/issues)
* [la vue du tableau](https://gitlab.com/Glenans-CDMEB/Projets/boards?=) (avec backlog, todo, doing, done)
* [la vue par *milestone* ](https://gitlab.com/Glenans-CDMEB/Projets/milestones) (une *milestone* = une étape = un trimestre)


