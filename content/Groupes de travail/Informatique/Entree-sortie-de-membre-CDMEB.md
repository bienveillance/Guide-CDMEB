---
title: "Entrée / sortie de membre CDMEB"
description: ""
---

Le but est de lister les tâches à effectuer pour les entrées / sorties de membres du CDMEB

## Sortie

* désactiver le compte Nextcloud (Drive)
* enlever de CDMEB Projets (Gitlab)
* supprimer de la liste `membres@©dmeb.org`
* ajouter sur la liste `sympathisants@cdmeb.org`

## Entrée

* Ajouter sur la liste `membres@©dmeb.org`
* Communiquer le Guide des outils numériques (à poser sur le futur site Guide.cdmeb.org)
* Créer le compte Nextcloud, et placer dans le groupe `cdmeb`