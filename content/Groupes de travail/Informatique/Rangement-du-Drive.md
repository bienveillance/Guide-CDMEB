---
title: "Rangement Du Drive"
date:  2018-01-15T15:16:23+01:00
weight: 5
---

Les "us et coutûmes" de qu'est-ce qui est rangé où dans le Drive.

## Dossier `Dossier-20xx/3 - Compte-rendus`

Contenu :

* Ordres du jour des réunions (format PDF ou bureautique)
* Compte-rendu de réunion (format PDF ou bureautique)
