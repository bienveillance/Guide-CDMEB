---
title: "Liste des sites web"
weight: 3
---

## Sites du CDMEB

Sites web:

* [`WWW.moniteurs.glenans.asso.fr` - Site Moniteurs](https://www.moniteurs.glenans.asso.fr/)
* [`DOC.moniteurs.glenans.asso.fr` - Base documentaire (~500 documents)](http://doc.moniteurs.glenans.asso.fr/) gérée par le CDMEB
* [`drive.cdmeb.org` - "Le drive" documents internes du CDMEB](http://drive.cdmeb.org/) Identifiants `visiteur`/`Glenans1947` ou identifiants individuels
* [`stats.cdmeb.org` - Statistiques web de nos sites](http://stats.cdmeb.org/) (accès restreint)

Réseaux sociaux :

* [Compte Twitter `@cdm_glenans`](https://twitter.com/CDM_Glenans/)
* [Page Facebook `facebook.com/moniteurs.glenans`](https://www.facebook.com/moniteurs.glenans/)
* [Compte Vimeo `vimeo.com/lesglenans`](https://vimeo.com/lesglenans)


## Sites officiels Glénans

Sites web:

* [`www.glenans.asso.fr` - Site Officiel Glénans](http://glenans.asso.fr/)

Réseaux sociaux "officiels" :

* [Compte Twitter `@Les_Glenans`](https://twitter.com/Les_Glenans)
* [Page Facebook `facebook.com/LesGlenans/`](https://www.facebook.com/LesGlenans/)
* [Chaine Youtube](https://www.youtube.com/channel/UCL_7p2XUlrvthgPlxFeUoRg)
* [Compte Instagram](https://www.instagram.com/lesglenans_officiel/)

## Pages non-officielles

* Pages LinkedIn (TODO à trouver)

Bases :

* Facebook [Les Glénans Marseillan](https://www.facebook.com/Les-Gl%C3%A9nans-Marseillan-179068912166395/)
* Facebook [Les Glénans - Centre de formation](https://www.facebook.com/glenansformation/)

Comités de secteur :

TODO