---
title: "Créer son compte sur DOC.moniteurs"
date:  2018-01-20T20:04:45+01:00
weight: 5
---

Pour pouvoir contribuer aux fiches de lecture, il convient d'avoir un compte. Voici les étapes à suivre pour avoir le sien.

1. Rendez-vous sur la page de [Création de compte du WordPress CDMEB](https://wp.cdmeb.org/wp-signup.php)
2. Suivez les instructions :
    * Choisissez un identifiant, typiquement pour "Pierre Yves Durand" choisir `pydurand`
    * Saississez votre courriel (nécessaire pour activer votre compte)
3. 

