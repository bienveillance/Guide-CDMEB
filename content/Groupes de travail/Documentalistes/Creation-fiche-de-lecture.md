---
title: "Création de fiche de lecture"
description: ""
---



(idées posées rapidement pour ne pas les oublier)

Éléments obligatoires :

* titre
* image d'illustration
* lien de téléchargement (vers le Drive Visiteur)

Éléments optionnels (mais bienvenus :) ):

* description (plus ou moins longue selon l'humeur du rédacteur)
* étiquettes (*tags*): de 0 à 4 (merci de ne pas dépasser 4, ceci incite à choisir les tags les plus pertinents)

## Pré-requis

* Avoir un compte sur DOC.moniteur (voir Créer son compte sur DOC.moniteurs)
